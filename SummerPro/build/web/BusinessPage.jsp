<%-- 
    Document   : BusinessPage
    Created on : Jun 30, 2016, 5:40:39 PM
    Author     : Not_the_One
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- Set width to the size of device--%>
        <meta name="viewpoint" content="width = device-width, initial-scale = 1">
        <title>Business</title>
        <%-- Bootstrap styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap.min.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>font-awesome-4.6.3/css/font-awesome.css />
        <%-- Custom styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>style.css />
    </head>
    <body class="bg-gray" style='overflow-x: hidden'>
        
        <div class="container">    
          <div class="row">
              <div class="col-lg-12 col-md-12 col-md-12 col-sm-12">
                  <m:Header pageType="2"></m:Header>
              </div> <%-- Header ends--%>
           
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">                  
                <m:LeftCorner pageType="3"></m:LeftCorner>                    
                </div><%-- Left Corner ends--%>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 top-element">
                <form role="form">
                    <div class="panel media bg-tr">
                     <div class="media-left">
                       <span class="glyphicon glyphicon-search infbtn color-white margin-top-big-icon" aria-hidden="true" style="margin-left: 20%"></span><br />                    
                     </div>
                    <div class="media-body">
                        <br/>
                        <textarea class="form-control" id="thought" cols='100' placeholder="Search..." style="width:95%;margin-left: 2%"></textarea>
                        <button type="submit" class="btn btn-sm bg-white color-tr float-right" style="margin: 1%;margin-right: 3%">Submit</button>

                    </div>
                 </div>
                </form>
                <%-- Searching post ends--%>
                                                     
              <div id="resspnl">                  
                                
              </div><%-- Results ends--%>
             </div>
                
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" >
                <m:RightCorner pageType="4"></m:RightCorner>
            </div><%-- Right Corner ends--%>
                
            </div><%-- Row ends--%>
        </div><%-- Container ends--%>
        
                
        <script src=<c:out value='${initParam["JSDirectory"]}'/>jquery-2.2.3.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>bootstrap.min.js></script>
    </body>
</html>

