<%@tag description="Login Panel" pageEncoding="UTF-8"%>

<div class="panel panel-tr top-element">
<div class="panel-heading panel-heading-tr">
   <ul class="nav nav-pills nav-pills-centered">     
    <li role="presentation" class="active inline-block">
        <a data-toggle="tab" href="#login" class="circle border-radius med-margin start-link">Login</a>
    </li>
    <li role="presentation" class="inline-block">
        <a data-toggle="tab" href="#signup" class="circle border-radius med-margin start-link">Sign up</a>
    </li>       
   </ul>
    </div>
    
  <div class="tab-content">
   <div id="login" class="tab-pane fade in active">   
    <div class="panel-body">       
    <form role="form">
        <div class="form-group small-font-size">
            <label for="emailID" style="margin: 15px 25px 5px">Email:</label>
          <center>
              <input type="email" class="form-control login-w bg-gray" id="emailID">
          </center>
        </div>
        <div class="form-group small-font-size">
          <label for="pwd" style="margin-left: 25px">Password:</label>
          <center>
              <input type="password" class="form-control login-w bg-gray" id="pwd">
          </center>
        </div>
        <div class="checkbox inline-block login-w small-font-size">
            <label class="start-label"><input type="checkbox" value="remember">Remember me</label>
            <a href="#" class="color-tr" style="margin-left: 15%;color:#31a2ac;">Forgot password?</a>
        </div>
        <center>
        <button type="submit" role="button" class="btn border-radius btn-secondary small-margin shadow">Log In</button>
        </center>
    </form>
    </div><%-- Panel body ends--%>    
    </div> <%-- Tab Content ends--%>
    
    <div id="signup" class="tab-pane fade">
    <div class="panel-body">
        <strong class="med-margin">Help</strong>
        <hr class="border-red small-margin">
        <p class="med-margin">If you do not have an Egimus account, you can sing up for one in a few steps:.</p><p></p>
        <p class="med-margin">Go to www.Egimus.com</p>
        <p class="med-margin">If you see the signup form, fill out your name, email address or phone number, password, birthday and gender. If you do
        not see the form, click the Sing Up, then fill out the form</p>
        <p class="med-margin">Click Sign Up.</p>
    </div> <%-- Panel body ends--%>
  </div> <%-- Tab Content ends--%>
  </div> <%-- Tab ends--%>
 
</div><%-- Panel ends--%>
