<%@tag description="Favorite things of User" pageEncoding="UTF-8"%>

<div class="panel">
    <div class="panel-heading text-danger">
     <a href="#">
     <strong class="text-danger">Favorite Things</strong></a>
     <hr class="border-red hr-margin">
    </div>
    <div class="panel-body">
        <ul class="circle-styling">
            <li>Favorite thing 1</li>
            <li>Favorite thing 2</li>
            <li>Favorite thing 3</li>
        </ul>
    </div>
</div>