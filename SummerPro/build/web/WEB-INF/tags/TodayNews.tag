<%@tag description="Today's News" pageEncoding="UTF-8"%>

<div class="panel news-panel-height">
    <div class="panel-heading text-danger"><a href="News.jsp">
            News</a><hr class="border-red no-margin">
    </div>
   <div class="panel-body overflow">
     <ul class="nav nav-tabs">
        <li class="active smalll-font-size"><a class="text-danger" data-toggle="tab" href="#latest">Latest</a></li>
        <li class="smalll-font-size"><a class="text-danger" data-toggle="tab" href="#holiday">Holidays</a></li>
        <li class="smalll-font-size"><a class="text-danger" data-toggle="tab" href="#event">Events</a></li>
     </ul>
       
       <div class="tab-content">
   <div id="latest" class="tab-pane fade in active">
   <div class="panel panel-default">
    <div class="panel-body panel-max-height overflow">
       
          <a class="color-black" href="#"><strong>Name</strong></a>           
          <hr class="hr-margin">
          <a class="color-black" href="#"><strong>Name</strong></a>
          <hr class="hr-margin">
          <a class="color-black" href="#"><strong>Name</strong></a>
          <hr class="hr-margin">
    </div>
    <div class="panel-footer">
      <div class="text-center">
          <button id="lnews" type="button" role="button" class="btn btn-block" onclick="funcNews(this.id)">Show more</button>
    </div>
   </div> 
   </div> <%-- Panel ends--%>
    </div> <%-- Tab Content ends--%>
    <div id="holiday" class="tab-pane fade">
      <div class="panel panel-default">
    <div class="panel-body">
     
        <a class="color-black" href="#"><strong>Name</strong></a>           
        <hr class="hr-margin">
        <a class="color-black" href="#"><strong>Name</strong></a>
        <hr class="hr-margin">
        <a class="color-black" href="#"><strong>Name</strong></a>
        <hr class="hr-margin">
    </div>
    <div class="panel-footer">
      <div class="text-center">
      <button id="hnews" type="button" role="button" class="btn btn-block" onclick="funcNews(this.id)">Show more</button>
    </div>
   </div> 
   </div> <%-- Panel ends--%>
  </div> <%-- Tab Content ends--%>
    <div id="event" class="tab-pane fade">
     <div class="panel panel-default">
    <div class="panel-body">
    
        <a class="color-black" href="#"><strong>Name</strong></a>           
        <hr class="hr-margin">
        <a class="color-black" href="#"><strong>Name</strong></a>
        <hr class="hr-margin">
        <a class="color-black" href="#"><strong>Name</strong></a>
        <hr class="hr-margin">
    </div>
    <div class="panel-footer">
      <div class="text-center">
      <button id="enews" type="button" role="button" class="btn btn-block" onclick="funcNews(this.id)">Show more</button>
    </div>
   </div> 
   </div> <%-- Panel ends--%>
   </div> <%-- Tab Content ends--%>
  </div> <%-- Tab ends--%>     
    </div> 
</div>