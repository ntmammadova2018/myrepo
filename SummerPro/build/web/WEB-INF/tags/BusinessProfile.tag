<%@tag description="User Profile in Business" pageEncoding="UTF-8"%>

<div class="panel panel-tr" style="height:50%">
   <div class="panel-heading panel-heading-tr">
     <a href="Profile.jsp">
     <strong class="color-white">DinoZaur Baxsheliyev</strong></a>
    </div>                        
    <div class="panel-body panel-max-height-llong overflow">
        <form role="form">
            <div class="form-group small-font-size">                          
              <div class="checkbox">
                <label><input type="checkbox" value="">Employment</label>
              </div>
              <div class="checkbox">
                <label><input type="checkbox" value="">Volunteer</label>
              </div>
            </div>
            <div class="form-group">
                <input type="text" class="form-control">
            </div>
            <div class="form-group">
                <div class="radio">
                  <label><input type="radio" name="optradio">New</label>
                </div>
                <div class="radio">
                  <label><input type="radio" name="optradio">Experienced</label>
                </div>
            </div>
        </form>
    </div>
</div>