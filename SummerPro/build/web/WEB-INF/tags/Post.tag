<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@attribute name="postType" description="Defines which Post type it should be" 
             required="true" rtexprvalue="true"%>

<form role="form">
    <div class="panel media bg-tr">
     <div class="media-left">
      <c:choose>
      <c:when test="${postType eq 1}" >
       <span class="glyphicon glyphicon-camera infbtn color-white margin-top-big-icon" aria-hidden="true" style="margin-left: 20%"></span><br />
       <span class="glyphicon glyphicon-facetime-video infbtn color-white" aria-hidden="true" style="margin-left: 20%"></span>
      </c:when>
      <c:when test="${postType eq 2}" >
       <span class="glyphicon glyphicon-ok infbtn color-white news-section-size margin-top-icon" aria-hidden="true" style="margin-left: 20%"></span><br />
       <span class="glyphicon glyphicon-remove infbtn color-white news-section-size" aria-hidden="true" style="margin-left: 20%"></span>
      </c:when>
      </c:choose>
     </div>
    <div class="media-body">
      <c:choose>
      <c:when test="${postType eq 1}" >
        <div>
            <a href="#"><span class="glyphicon glyphicon-search small color-white float-right" aria-hidden="true" style="margin: 1% 3% 1%"></span></a>
        </div>
      </c:when>
      </c:choose>
        <br/>
        <input type="text" class="form-control" id="thought" placeholder="What's happening?" style="width:95%;height:55px;margin-left: 2%">
        <button type="submit" class="btn btn-sm bg-white color-tr float-right" style="margin: 1%;margin-right: 3%">Submit</button>     
    </div>
 </div>
   <%--<div class="panel bg-tr form-group" style="display:table;">
    <div class="panel-heading clearfix" style="display:table-cell;">    
    <c:choose>
    <c:when test="${postType eq 1}" >
    <span class="glyphicon glyphicon-camera infbtn color-white margin-top-big-icon" aria-hidden="true"></span><br />
    <span class="glyphicon glyphicon-facetime-video infbtn color-white" aria-hidden="true"></span>
    </c:when>
    <c:when test="${postType eq 2}" >
    <span class="glyphicon glyphicon-ok infbtn color-white news-section-size margin-top-icon" aria-hidden="true"></span><br />
    <span class="glyphicon glyphicon-remove infbtn color-white news-section-size" aria-hidden="true"></span>
    </c:when>
    </c:choose>
    </div>
     <div class="panel-body inline-block" id="thoughtbody">
      <a href="#"><span class="glyphicon glyphicon-search small color-white float-right" aria-hidden="true" style="margin-bottom: 1%;"></span></a>
      <textarea class="form-control" id="thought" cols='100' placeholder="What's happening?"></textarea>
      <button type="submit" class="btn btn-sm bg-white color-tr float-right" style="margin-bottom: -2%;margin-top: 1%;">Submit</button>
     </div>
    </div>--%>
</form> 