<%@tag description="Profile of the User" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="panel panel-tr">
    <div class="panel-heading panel-heading-tr">
     <a href="Profile.jsp">
     <div class="inline-block"><img class="img-size"
      src="http://science-all.com/images/wallpapers/random-image/random-image-6.jpg"
      alt="Profile photo"></div>
     <strong class="color-white">DinoZaur Baxsheliyev</strong></a>
    </div>
    <div class="panel-body">
        <ul class="unstyled">
            <li class="color-tr"><span class="color-black">Birthday</span> 29.11.1995</li>
            <li class="color-tr"><span class="color-black">Job</span> Own castle</li>
            <li class="color-tr"><span class="color-black">Education</span> The Science and Practice of Fire in Hogwarts</li>
            <li class="color-tr"><span class="color-black">Location</span> Baku, Azerbaijan</li>
            <li class="color-tr"><span class="color-black">See my</span> <a href="#" class="news">resume</a></li>
        </ul>
    </div>
</div>