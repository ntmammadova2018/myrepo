<%@tag description="Left Corner of the Page" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>

<%@attribute name="pageType" description="Defines which components should be used on the right side" 
             required="true" rtexprvalue="true"%>

<div class="sidebar-nav-fixed affix affix-w side-overflow top-element"  style="margin-left: -10px">      
 <div class="panel-group">
  <div class="panel panel-default">
    <div class="panel-heading" style="padding: 0">
         <ul class="nav nav-pills bg-tr" id="accordion">
        <li>
         <a class="tr-hover collapse" data-toggle="collapse" data-parent="#accordion" href="#friend"><span class="fa fa-user-plus lcbtn icon-size color-white" aria-hidden="true"></span></a>
        </li>
        <li>
         <a class="tr-hover collapse" data-toggle="collapse" data-parent="#accordion" href="#message"><span class="fa fa-envelope lcbtn icon-size color-white" aria-hidden="true"></span></a>
        </li>
        <li>
         <a class="tr-hover collapse" data-toggle="collapse" data-parent="#accordion" href="#notification"><span class="fa fa-exclamation-circle lcbtn icon-size color-white" aria-hidden="true"></span></a>
        </li>
        <li class="pull-right">
         <a class="tr-hover collapse" data-toggle="collapse" data-parent="#accordion" href="#setting"><span class="fa fa-cog lcbtn icon-size color-white" aria-hidden="true"></span></a>
        </li>
       </ul>
    </div>
      
    <div id="friend" class="panel-collapse collapse bg-white">                    
        <div class="panel-body">
            <div style="margin-bottom: 5%;">
                Friend Requests
                <a href="#" class="news"><div class="inline-block float-right">Find Friends</div></a>
           </div>
            <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img class="img-circle thumb-size"
                 src="https://s-media-cache-ak0.pinimg.com/236x/a7/61/d3/a761d340d87d888075314872a9b0c56e.jpg"
                 alt="Profile photo">
                      </a>
                    </div>
                    <div class="media-body">
                        <div class="media-heading inline-block"><strong>At</strong></div>
                      <span class="float-right inline-block">
                        <span class="fa-stack">
                            <i class="fa fa-check fa-stack-1x"></i>
                            <i class="fa fa-square-o fa-stack-2x color-tr"></i>
                        </span>
                        <span class="fa-stack">
                            <i class="fa fa-remove fa-stack-1x"></i>
                            <i class="fa fa-square-o fa-stack-2x color-tr"></i>
                        </span>
                        </span>
                    </div>
                  </div>
            <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img class="img-circle thumb-size"
                    src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTuYQx10k_h5CM5sr6-993XhKnBUbrj_9uJ1iyASDmqimlSEieD"
                    alt="Profile photo">
                      </a>
                    </div>
                    <div class="media-body">
                        <div class="media-heading inline-block"><strong>Ordek</strong></div>
                      <span class="float-right inline-block">
                        <span class="fa-stack">
                            <i class="fa fa-check fa-stack-1x"></i>
                            <i class="fa fa-square-o fa-stack-2x color-tr"></i>
                        </span>
                        <span class="fa-stack">
                            <i class="fa fa-remove fa-stack-1x"></i>
                            <i class="fa fa-square-o fa-stack-2x color-tr"></i>
                        </span>
                        </span>
                    </div>
                  </div>
            <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img class="img-circle thumb-size"
                    src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTN4qC24wGa4by7RoWSXEl5Rhsj7D6-NpXzOwh3G6qg_6UfXY-D2Q"
                    alt="Profile photo">
                      </a>
                    </div>
                    <div class="media-body">
                        <div class="media-heading inline-block"><strong>Zurafe</strong></div>
                      <span class="float-right inline-block">
                        <span class="fa-stack">
                            <i class="fa fa-check fa-stack-1x"></i>
                            <i class="fa fa-square-o fa-stack-2x color-tr"></i>
                        </span>
                        <span class="fa-stack">
                            <i class="fa fa-remove fa-stack-1x"></i>
                            <i class="fa fa-square-o fa-stack-2x color-tr"></i>
                        </span>
                        </span>
                    </div>
                  </div>                                                                            
            </div>
      </div><%-- Friend Request Section ends --%>
      
      <div id="message" class="panel-collapse collapse bg-white">                    
        <div class="panel-body">
                <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img class="img-circle thumb-size"
                 src="https://s-media-cache-ak0.pinimg.com/236x/a7/61/d3/a761d340d87d888075314872a9b0c56e.jpg"
                 alt="Profile photo">
                      </a>
                    </div>
                    <div class="media-body">
                        <div class="media-heading"><strong>At</strong></div>
                      Wanna run with me?
                    </div>
                  </div>
                  
                  <div class="media">
                    <div class="media-left">
                      <a href="#">
                        <img class="img-circle thumb-size"
                    src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTuYQx10k_h5CM5sr6-993XhKnBUbrj_9uJ1iyASDmqimlSEieD"
                    alt="Profile photo">
                      </a>
                    </div>
                    <div class="media-body">
                        <div class="media-heading"><strong>Ordek</strong></div>
                      I know a very good place for it!
                    </div>
                  </div>
                  
                  <div class="media">
                    <div class="media-left">
                      <a href="#">                        
                      <img class="img-circle thumb-size"
                    src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTN4qC24wGa4by7RoWSXEl5Rhsj7D6-NpXzOwh3G6qg_6UfXY-D2Q"
                    alt="Profile photo">
                      </a>
                    </div>
                    <div class="media-body">
                        <div class="media-heading"><strong>Zurafe</strong></div>
                      Teach me how to fire..
                    </div>
                  </div>
        </div>
      </div><%-- Message Section ends --%>
      
    <div id="setting" class="panel-collapse collapse bg-white">
        <div class="panel-body">
            <ul class="nav gray-hover">
                <li class="hr-margin"><a href="Settings.jsp" id="privacylink" onclick="openCollapse(this.id)">Privacy</a></li>
                <li class="hr-margin"><a href="Settings.jsp" id="accountlink" onclick="openCollapse(this.id)">Account Settings</a></li>
                <li class="hr-margin"><a href="Settings.jsp" id="blocklink" onclick="openCollapse(this.id)">Blocking</a></li>
                <li class="hr-margin"><a href="Settings.jsp" id="helplink" onclick="openCollapse(this.id)">Help Desk</a></li>
            </ul>
        </div>
    </div><%-- Settings Section ends --%>
    </div>
    </div><%-- Header of Right Corner ends --%>
    <c:choose>
    <c:when test="${pageType eq 1}" >
        <m:TodayNews></m:TodayNews>        
        <m:PopularDebate></m:PopularDebate>
    </c:when>
    <c:when test="${pageType eq 2}" >
        <m:FavoriteThings></m:FavoriteThings>
        <m:UserPhoto></m:UserPhoto>
    </c:when>
    <c:when test="${pageType eq 3}" >
        
    </c:when>
    <c:when test="${pageType eq 4}" >
        <m:TopCV></m:TopCV>
    </c:when>
    </c:choose>
</div>
    
    <%-- Modal for debate creation --%>
    <div class="modal fade" id="createDebate" role="dialog">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <img src="..." alt="Image for debate" />
          </div>
          <div class="modal-body">
              <form>
                  <div class="form-group">
                    <label for="argument">Write your argument</label>
                    <input type="text" class="form-control" id="argument">
                  </div>
                  <div class="form-group">
                    <label for="explanation">Explanation of your argument</label>
                    <textarea class="form-control" id="explanation"></textarea>
                  </div>
                  <div class="form-group">
                    <label>Select privacy:</label>
                    <div class="radio">
                      <label><input type="radio" name="public">Public</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="friends">Friends</label>
                    </div>
                  </div>
              </form>
          </div>
            <div class="modal-footer bg-tr" style="border-bottom-left-radius: 5px;border-bottom-right-radius: 5px;">
            <button type="button" class="btn btn-default border-radius" data-dismiss="modal" style="border-radius: 10% !important;">Save</button>
            <button type="button" class="btn btn-default border-radius" data-dismiss="modal" style="border-radius: 10% !important;">Close</button>
          </div>
        </div>
      </div>
    </div><%-- Modal ends--%>