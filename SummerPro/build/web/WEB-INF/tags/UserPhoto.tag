<%@tag description="Photos of the User in the Profile" pageEncoding="UTF-8"%>

<div class="panel">
    <div class="panel-heading text-danger">
     <a href="#">
     <strong class="text-danger">Photos</strong></a>
     <hr class="border-red hr-margin">
    </div>
    <div class="panel-body inline-block">
      <a class="thumbnail photo-display">
      <img src="..." alt="Photo 1">
      </a><a class="thumbnail photo-display">
      <img src="..." alt="Photo 2">
      </a><a class="thumbnail photo-display">
      <img src="..." alt="Photo 3">
      </a><a class="thumbnail photo-display">
      <img src="..." alt="Photo 4">
      </a><a class="thumbnail photo-display">
      <img src="..." alt="Photo 5">
      </a><a class="thumbnail photo-display">
      <img src="..." alt="Photo 6">
      </a>
    </div>
</div>