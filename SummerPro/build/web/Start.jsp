<%-- 
    Document   : Start
    Created on : 14.06.2016, 0:04:48
    Author     : Not_the_One
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- Set width to the size of device--%>
        <meta name="viewpoint" content="width = device-width, initial-scale = 1">
        <title>Egimus</title>
        <%-- Bootstrap styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap.min.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>font-awesome-4.6.3/css/font-awesome.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap-select-1.10.0/dist/css/bootstrap-select.min.css />
        <%-- Custom styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>style.css />
        <!--[if lt IE 9]>
        <script type="text/javascript" src="https://raw.githubusercontent.com/aFarkas/html5shiv/master/dist/html5shiv-printshiv.min.js"></script>
        <![endif]-->
        <style>
            .nav-pills > li > a:link{
            background-color: #2f2f2b !important;
            color: #ffffff;
            opacity: 0.6;
            }    
            .nav-pills > li > a:hover,a:visited{
            background-color: #2f2f2b !important;
            color: #ffffff;
            opacity: 1;
            }
            .nav-pills > .active > a{
            background-color: black !important;
            text-decoration: none;
            color: #ffffff;
            opacity: 1 !important;
            }
        </style>
    </head>
    <body class="bg-gray" style='overflow-x: hidden'>
        
        <div class="container">    
          <div class="row">
              <div class="col-lg-12 col-md-12 col-md-12 col-sm-12">
                <m:Header pageType="2"></m:Header>
              </div> <%-- Header ends--%>              
                
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 top-element">                    
                  <div class="panel panel-tr">
                      <div class="panel-heading nav panel-heading-tr">
                        <strong class="color-white icon-size">Join us for the beautiful world!</strong>
                        <div class="float-right nav navbar-nav">
                            <div class="form-group">
                                <label for="lang" class="color-white">Language</label>
                                <select class="selectpicker" id="lang" data-width="60%">
                                  <option>Azerbaijani</option>
                                  <option>English</option>
                                  <option>Russian</option>
                                  <option>Spanish</option>
                                  <option>Turkish</option>
                                </select>
                            </div>
                        </div>
                      </div><%-- Panel Heading ends --%>
                      <div  id="loginPage">
                      <div class="panel-body">
                          <div class="image">
                                  <h2 style='position: absolute; top: 10%;right: 5%;'>
                                      Don't waste your time for useless things.<br/> <br/>
                                      <span style="position:absolute;right:0">Be online, be creative.</span>
                                  </h2>
                          <img id="background-login"
                           src=<c:out value='${initParam["CSSDirectory"]}'/>background_ui.gif alt="Background Image"
                           class=''
                           style="width:100%;height:100%;">
                          </div>
                          
                      </div>
                      <div class="panel-footer bg-white" style="height:25%">
                          About<hr class="border-red">
                          <p>Founded in 2016, Egimus' mission is to give people the power to share and make
                           the world more open and connected with friends and family, to discover what's going on in the world,
                          and to share and express what matters to them.</p>
                      </div>
                      </div>
                      <div id="signupPage">
                          <br /><br />
                          <form role="form">
                        <div class="form-group small-font-size">
                          <center>
                            <label for="nameID" class="control-label inline-block start-w start-label">Name:</label>                          
                            <input type="text" class="form-control login-input-w bg-gray inline-block" id="nameID" autofocus required>
                          </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                            <label for="snameID" class="control-label inline-block start-w start-label">Surname:</label>                          
                            <input type="text" class="form-control login-input-w bg-gray inline-block" id="snameID" required>
                          </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                            <label for="emailID" class="control-label inline-block start-w start-label">Email:</label>                          
                            <input type="email" class="form-control login-input-w bg-gray inline-block" id="emailID" required>
                          </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                           <label for="phoneID" class="control-label inline-block start-w start-label">Phone:</label>                          
                           <input type="number" class="form-control login-input-w bg-gray inline-block" id="phoneID">
                          </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                            <label for="bdID" class="control-label inline-block start-w start-label">Birthday:</label>
                            <select class="selectpicker" id="bdID" data-width="5%" data-style="btn-secondary" required>
                                <option>1</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                                <option>6</option>
                                <option>7</option>
                                <option>8</option>
                                <option>9</option>
                                <option>10</option>
                                <option>11</option>
                                <option>12</option>
                                <option>13</option>
                                <option>14</option>
                                <option>15</option>
                                <option>16</option>
                                <option>17</option>
                                <option>18</option>
                                <option>19</option>
                                <option>20</option>
                                <option>21</option>
                                <option>22</option>
                                <option>23</option>
                                <option>24</option>
                                <option>25</option>
                                <option>26</option>
                                <option>27</option>
                                <option>28</option>
                                <option>29</option>
                                <option>30</option>
                                <option>31</option>
                            </select>
                            <select class="selectpicker" id="bdID" data-width="20%" data-style="btn-secondary" required>
                                <option>January</option>
                                <option>February</option>
                                <option>March</option>
                                <option>April</option>
                                <option>May</option>
                                <option>June</option>
                                <option>July</option>
                                <option>August</option>
                                <option>September</option>
                                <option>October</option>
                                <option>November</option>
                                <option>December</option>
                            </select>
                            <select class="selectpicker" id="bdID" data-width="10%" data-style="btn-secondary" required>
                                <option>1990</option>
                                <option>1991</option>
                                <option>1992</option>
                                <option>1993</option>
                                <option>1994</option>                                
                                <option>1995</option>
                                <option>1996</option>
                                <option>1997</option>
                                <option>1998</option>                                
                                <option>1999</option>
                                <option>2000</option>
                                <option>2001</option>
                                <option>2002</option>
                            </select>
                          </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                              <label for="genderID" class="control-label start-w start-label">Gender:</label>
                              <select class="selectpicker" id="genderID" data-width="35%" data-style="btn-secondary" required>
                                  <option>Female</option>
                                  <option>Male</option>
                                  <option>Other</option>
                                </select>                            
                            </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                            <label for="countryID" class="control-label start-w start-label">Country:</label>
                            <select class="selectpicker" id="countryID" data-width="35%" data-style="btn-secondary" required>
                                <option>Azerbaijan</option>
                                <option>Great Britain</option>
                                <option>Russia</option>
                                <option>Spain</option>
                                <option>Turkey</option>
                              </select>
                            </center>
                        </div>                        
                        <div class="form-group small-font-size">
                          <center>
                            <label for="passwordID" class="control-label inline-block start-w start-label">Password:</label>
                            <input  title="Password should contain at least one capital letter and a number, and be more than 8 letters." 
                                type="password" pattern="[^'\x22\?\/](?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}[^'\x22\?\/]" class="form-control login-input-w bg-gray inline-block" id="passwordID" required>
                          </center>
                        </div>                        
                        <div class="form-group small-font-size">
                          <center>
                            <label for="password2ID" class="control-label inline-block start-w start-label">Confirm Password:</label>
                            <input title="Reenter the previous password."
                                type="password" pattern="[^'\x22\?\/](?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}[^'\x22\?\/]" class="form-control login-input-w bg-gray inline-block" id="password2ID" required>
                          </center>
                        </div>
                        <center style="margin-left: 8%;">
                        <div class="checkbox">
                        <label class="start-label"><input type="checkbox" value="">I agree with your Terms</label>
                        </div>
                       </center>   
                       <hr class="hr-margin border-red">                                       
                           <button type="submit" role="button" class="btn border-radius bg-tr color-white shadow" style="margin-left:75%;margin-bottom: 2%;">Sign Up</button>
                        
                    </form>
                      </div>
                </div><%-- Panel ends--%>
              </div><%-- Main section ends--%>
                
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" >
              <m:Login></m:Login>
            </div><%-- Login Section ends--%>
                
            </div><%-- Row ends--%>
        </div><%-- Container ends--%>
        
                
        <script src=<c:out value='${initParam["JSDirectory"]}'/>jquery-2.2.3.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>bootstrap.min.js></script>
        <script src=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap-select-1.10.0/dist/js/bootstrap-select.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>LoginJS.js></script>
    </body>
</html>

