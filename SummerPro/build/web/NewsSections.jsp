<%-- 
    Document   : NewsSections
    Created on : Jun 27, 2016, 2:53:26 PM
    Author     : Not_the_One
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- Set width to the size of device--%>
        <meta name="viewpoint" content="width = device-width, initial-scale = 1">
        <title>News</title>
        <%-- Bootstrap styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap.min.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>font-awesome-4.6.3/css/font-awesome.css />
        <%-- Custom styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>style.css />
    </head>
    <body class="bg-gray" style='overflow-x: hidden'>
        
        <div class="container">    
          <div class="row">
              <div class="col-lg-12 col-md-12 col-md-12 col-sm-12">
                <m:Header pageType="1"></m:Header>
              </div> <%-- Header ends--%>
           
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">                  
                <m:LeftCorner pageType="1"></m:LeftCorner>                    
                </div><%-- Right Corner ends--%>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 top-element">                    
                  <div class="panel panel-tr">
                      <div class="panel-heading panel-heading-tr">
                      <strong class="color-white icon-size">
                       <c:choose>
                         <c:when test="${param.newsType eq 1}" >
                        Latest News
                         </c:when>
                        <c:when test="${param.newsType eq 2}" >
                        Holidays
                         </c:when>
                        <c:when test="${param.newsType eq 3}" >
                        Events
                         </c:when>
                       </c:choose>
                      </strong>
                      </div>
                      <div class="panel-body">
                          <div id="newsCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Indicators -->
                        <ol class="carousel-indicators">
                          <li data-target="#newsCarousel" data-slide-to="0" class="active"></li>
                          <li data-target="#newsCarousel" data-slide-to="1"></li>
                          <li data-target="#newsCarousel" data-slide-to="2"></li>
                          <li data-target="#newsCarousel" data-slide-to="3"></li>
                        </ol>

                        <%-- Wrapper for slides --%>
                        <div class="carousel-inner" role="listbox">
                          <div class="item active news-carousel-size">
                            <img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcQHsXtR7h3ALfssinIg4nMKpZqluA48kP8zhz4bdvycad9j-7pJ"
                             alt="News" class="news-carousel-size">
                            <strong>A title of news</strong>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing<a class="news" href="#"> more...</a></p>
                          </div>

                          <div class="item news-carousel-size">
                            <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcS7A4_ObsA24brn-Owb_osKTUgsPJcPYpZZ-oVd62YAgZvrdVDRUw"
                             alt="News" class="news-carousel-size">
                          </div>

                          <div class="item news-carousel-size">
                            <img src="https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSplwSU6nmhsp2N6KkPiB_2JWY0YyNuh977Tsg1D01RkN4_7xBu"
                             alt="News" class="news-carousel-size">
                          </div> 

                          <div class="item news-carousel-size">
                            <img src="https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcRUrYbdrEyQ83QKQ-edHrKg2Gj_2hBTet8HOQ7CgJsVjNJiXtJkJQ"
                              alt="News" class="news-carousel-size">
                          </div>
                        </div>

                        <%-- Left and right controls --%>
                        <a class="left carousel-control" href="#newsCarousel" role="button" data-slide="prev">
                          <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#newsCarousel" role="button" data-slide="next">
                          <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                        </a>
                      </div>
                     </div>                       
                </div><%-- Panel ends--%>
              </div><%-- News ends--%>
             <%-- News feed ends--%>
                
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" >
              <m:RightCorner pageType="1"></m:RightCorner>
            </div><%-- Debate Section ends--%>
                
            </div><%-- Row ends--%>
        </div><%-- Container ends--%>
        
                
        <script src=<c:out value='${initParam["JSDirectory"]}'/>jquery-2.2.3.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>bootstrap.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>functions.js></script>
    </body>
</html>

