<%-- 
    Document   : Debate
    Created on : Jun 9, 2016, 7:32:20 PM
    Author     : Not_the_One
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- Set width to the size of device--%>
        <meta name="viewpoint" content="width = device-width, initial-scale = 1">
        <title>Home</title>
        <%-- Bootstrap styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap.min.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>font-awesome-4.6.3/css/font-awesome.css />
        <%-- Custom styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>style.css />
    </head>
    <body class="bg-gray" style='overflow-x: hidden'>
        
        <div class="container">    
          <div class="row">
              <div class="col-lg-12 col-md-12 col-md-12 col-sm-12">
                <m:Header pageType="1"></m:Header>
              </div> <%-- Header ends--%>
           
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">                  
                <m:LeftCorner pageType="1"></m:LeftCorner>                    
                </div><%-- Right Corner ends--%>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 top-element">
                    <img  class="thumbnail cover-size" src=<c:out value='${initParam["CSSDirectory"]}'/>default-cover-picture-2.png
                          alt="Cover photo">                    
                                    
                  <div class="panel">
                      <div class="panel-heading">
                          <strong>Definitely a title of a Debate</strong>
                      </div>
                        <div class="panel-body" id="newspnlbody">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.</p>                                                                                  
                        </div>                   
                   <div class="panel-footer small-height">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-check" aria-hidden="true"></i> Agree</button>
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-close" aria-hidden="true"></i> Disagree</button>
                            <button type="button" role="button" class="btn no-border no-bg float-right">
                                <i class="fa fa-search" aria-hidden="true"></i> Search</button>
                            </div>
                        </div>
                      </form>
                   </div>                  
              </div><%-- Debate Info ends--%>
              
            <div class="progress">
              <div class="progress-bar bg-tr" style="width: 60%">
                 60%
              </div>
              <div class="progress-bar bg-red" style="width: 40%">
                 40%
              </div>
            </div><%-- Progress Banner ends--%>
              
              <m:Post postType="2"></m:Post>                                     
                <%-- Post ends--%>
                
                <div class="panel">
                    <div class="panel-heading">
                        <a href="Profile.jsp">
                        <div class="inline-block"><img class="img-circle thumb-size"
                    src="http://science-all.com/images/wallpapers/random-image/random-image-6.jpg"
                    alt="Profile photo"></div>
                      <strong>DinoZaur Baxsheliyev</strong></a>
                    </div>
                    <div class="panel-body">                            
                     <p>Status/Photo</p>                                                                                  
                     </div>                   
                   <div class="panel-footer small-height">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-check" aria-hidden="true"></i></button>
                                <i class="fa fa-at" aria-hidden="true"></i></button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div><%-- first comment ends--%>
             </div><%-- News feed ends--%>
                
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" >
              <m:RightCorner pageType="1"></m:RightCorner>
            </div><%-- Debate Section ends--%>
                
            </div><%-- Row ends--%>
        </div><%-- Container ends--%>
        
                
        <script src=<c:out value='${initParam["JSDirectory"]}'/>jquery-2.2.3.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>bootstrap.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>functions.js></script>
    </body>
</html>
