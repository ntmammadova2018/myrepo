<%-- 
    Document   : Home
    Created on : May 16, 2016, 3:39:57 PM
    Author     : Not_the_One
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- Set width to the size of device--%>
        <meta name="viewpoint" content="width = device-width, initial-scale = 1">
        <title>Home</title>
        <%-- Bootstrap styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap.min.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>font-awesome-4.6.3/css/font-awesome.css />
        <%-- Custom styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>style.css />
    </head>
    <body class="bg-gray" style='overflow-x: hidden'>
        
        <div class="container">    
          <div class="row">
              <div class="col-lg-12 col-md-12 col-md-12 col-sm-12">
                <m:Header pageType="1"></m:Header>
              </div> <%-- Header ends--%>
           
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">                  
                <m:LeftCorner pageType="1"></m:LeftCorner>                    
                </div><%-- Left Corner ends--%>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 top-element">               
                    <m:Post postType="1"></m:Post>                                     
                <%-- Post ends--%>
                                    
                  <div class="panel" id="newspnl">                  
                        <div class="panel-body" id="newspnlbody">
                            <strong class="text-danger">Name'n'Photo</strong>
                            <p>Status/Photo</p>                                                                                  
                        </div>                   
                   <div class="panel-footer">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <a href="#" onclick="return liked();">                               
                                <svg width="30px" height="30px" class="inline-block">
                                <image href=<c:out value='${initParam["CSSDirectory"]}'/>logoboznutton.svg id="likeButton" x="-15" y="-15" height="60" width="60" />
                                </svg>
                            </a>
                            <button type="button" role="button" class="btn no-border no-bg" style="margin-top: -25px;">
                                <i class="fa fa-commenting icon-size" aria-hidden="true"></i></button>                           
                            <button type="button" role="button" class="btn pbtn no-border no-bg float-right">
                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i> Make it a debate topic</button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div><%-- first panel ends--%>
                     <div class="panel" id="newspnl">                  
                        <div class="panel-body" id="newspnlbody">
                            <strong class="text-danger">Name'n'Photo</strong>
                            <p>Status/Photo</p>                                                                                  
                        </div>                   
                   <div class="panel-footer">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>Like</button>
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-commenting" aria-hidden="true"></i> Write</button>                           
                            <button type="button" role="button" class="btn pbtn no-border no-bg  float-right">
                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i> Make it a debate topic</button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div><%-- second panel ends--%>
                <div class="panel" id="newspnl">                  
                        <div class="panel-body" id="newspnlbody">
                            <strong class="text-danger">Name'n'Photo</strong>
                            <p>Status/Photo</p>                                                                                  
                        </div>                   
                   <div class="panel-footer">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i> Like</button>
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-commenting" aria-hidden="true"></i> Write</button>                           
                            <button type="button" role="button" class="btn pbtn no-border no-bg  float-right">
                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i> Make it a debate topic</button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div><%-- 3rd panel ends--%>
                <div class="panel" id="newspnl">                  
                        <div class="panel-body" id="newspnlbody">
                            <strong class="text-danger">Name'n'Photo</strong>
                            <p>Status/Photo</p>                                                                                  
                        </div>                   
                   <div class="panel-footer">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i> Like</button>
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-commenting" aria-hidden="true"></i> Write</button>                           
                            <button type="button" role="button" class="btn pbtn no-border no-bg  float-right">
                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i> Make it a debate topic</button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div>
                <div class="panel" id="newspnl">                  
                        <div class="panel-body" id="newspnlbody">
                            <strong class="text-danger">Name'n'Photo</strong>
                            <p>Status/Photo</p>                                                                                  
                        </div>                   
                   <div class="panel-footer">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i> Like</button>
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-commenting" aria-hidden="true"></i> Write</button>                           
                            <button type="button" role="button" class="btn pbtn no-border no-bg  float-right">
                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i> Make it a debate topic</button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div>
                <div class="panel" id="newspnl">                  
                        <div class="panel-body" id="newspnlbody">
                            <strong class="text-danger">Name'n'Photo</strong>
                            <p>Status/Photo</p>                                                                                  
                        </div>                   
                   <div class="panel-footer">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i> Like</button>
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-commenting" aria-hidden="true"></i> Write</button>                           
                            <button type="button" role="button" class="btn pbtn no-border no-bg  float-right">
                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i> Make it a debate topic</button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div>
                <div class="panel" id="newspnl">                  
                        <div class="panel-body" id="newspnlbody">
                            <strong class="text-danger">Name'n'Photo</strong>
                            <p>Status/Photo</p>                                                                                  
                        </div>                   
                   <div class="panel-footer">
                       <form role="form">
                        <div class="form-group">
                            <div class="text-left">
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i> Like</button>
                            <button type="button" role="button" class="btn no-border no-bg">
                                <i class="fa fa-commenting" aria-hidden="true"></i> Write</button>                           
                            <button type="button" role="button" class="btn pbtn no-border no-bg  float-right">
                                <i class="fa fa-long-arrow-up" aria-hidden="true"></i> Make it a debate topic</button>
                            </div>
                        </div>
                      </form>
                   </div>
                </div>
              </div><%-- News ends--%>
             <%-- News feed ends--%>
                
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" >
              <m:RightCorner pageType="1"></m:RightCorner>
            </div><%-- Right Corner Section ends--%>
                
            </div><%-- Row ends--%>
        </div><%-- Container ends--%>
        
       
        <script src=<c:out value='${initParam["JSDirectory"]}'/>jquery-2.2.3.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>bootstrap.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>functions.js></script>
        
         <script>
        $('.collapse').on('show.bs.collapse', function () {
            var previousCollapse = $(this);
            $('.collapse').on('show.bs.collapse', function () {
                previousCollapse.removeClass('in');
            });
        });
        </script>
    </body>
</html>
