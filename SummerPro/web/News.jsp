<%-- 
    Document   : News
    Created on : Jun 9, 2016, 5:22:26 PM
    Author     : Not_the_One
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- Set width to the size of device--%>
        <meta name="viewpoint" content="width = device-width, initial-scale = 1">
        <title>News</title>
        <%-- Bootstrap styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap.min.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>font-awesome-4.6.3/css/font-awesome.css />
        <%-- Custom styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>style.css />
    </head>
    <body class="bg-gray" style='overflow-x: hidden'>
        
        <div class="container">    
          <div class="row">
              <div class="col-lg-12 col-md-12 col-md-12 col-sm-12">
                <m:Header pageType="1"></m:Header>
              </div> <%-- Header ends--%>
           
                <div class="col-lg-3 col-md-3 hidden-sm hidden-xs">                  
                <m:LeftCorner pageType="1"></m:LeftCorner>                    
                </div><%-- Right Corner ends--%>
                
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 top-element">                    
                  <div class="panel panel-tr">
                      <div class="panel-heading panel-heading-tr nav">
                        <strong class="color-white icon-size inline-block">News</strong>
                        <form role="form" class="inline-block float-right nav navbar-nav">
                          <input id="newsdate" type="date" class="form-control">
                        </form>
                      </div>
                      <div class="panel-body news-height overflow">
                        <strong class="news-section-size">Latest News</strong>
                        <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object news-size"
                        src="..."
                        alt="News photo">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><strong>Latinish letters</strong></div>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.<a class="news" href="#"> more...</a>
                        </div>
                      </div>
                        <hr class="hr-margin">
                        <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object news-size"
                        src="..."
                        alt="News photo">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><strong>Definitely a title</strong></div>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.<a class="news" href="#"> more...</a>
                        </div>
                      </div>                        
                      </div>
                      <hr class="text-danger border-red no-margin">
                      <div class="panel-body news-height overflow">                           
                        <strong class="news-section-size">Holidays</strong>
                         <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object news-size"
                        src="..."
                        alt="News photo">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><strong>Latinish holidays coming soon</strong></div>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.<a class="news" href="#"> more...</a>
                        </div>
                      </div>
                        <hr class="hr-margin">
                        <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object news-size"
                        src="..."
                        alt="News photo">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><strong>Kinda..</strong></div>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.<a class="news" href="#"> more...</a>
                        </div>
                      </div>
                      </div>
                      <hr class="text-danger border-red no-margin">
                      <div class="panel-body news-height overflow">
                        <strong class="news-section-size">Events</strong>
                        <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object news-size"
                        src="..."
                        alt="News photo">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><strong>2014</strong></div>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.<a class="news" href="#"> more...</a>
                        </div>
                      </div>
                        <hr class="hr-margin">
                        <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object news-size"
                        src="..."
                        alt="News photo">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><strong>2015</strong></div>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.<a class="news" href="#"> more...</a>
                        </div>
                      </div>
                        <div class="media">
                        <div class="media-left">
                          <a href="#">
                            <img class="media-object news-size"
                        src="..."
                        alt="News photo">
                          </a>
                        </div>
                        <div class="media-body">
                            <div class="media-heading"><strong>2016</strong></div>
                          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt
                            ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation 
                            ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in 
                            reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. 
                            Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt 
                            mollit anim id est laborum.<a class="news" href="#"> more...</a>
                        </div>
                      </div>
                      </div>
                </div><%-- Panel ends--%>
              </div><%-- News ends--%>
             <%-- News feed ends--%>
                
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" >
              <m:RightCorner pageType="1"></m:RightCorner>
            </div><%-- Debate Section ends--%>
                
            </div><%-- Row ends--%>
        </div><%-- Container ends--%>
        
                
        <script src=<c:out value='${initParam["JSDirectory"]}'/>jquery-2.2.3.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>bootstrap.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>functions.js></script>
    </body>
</html>
