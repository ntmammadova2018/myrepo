<%-- 
    Document   : Settings
    Created on : Jun 27, 2016, 5:43:47 PM
    Author     : Not_the_One
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%-- Set width to the size of device--%>
        <meta name="viewpoint" content="width = device-width, initial-scale = 1">
        <title>Settings</title>
        <%-- Bootstrap styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap.min.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>font-awesome-4.6.3/css/font-awesome.css />
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap-select-1.10.0/dist/css/bootstrap-select.min.css />
        <%-- Custom styling--%>
        <link rel="stylesheet" type="text/css" href=<c:out value='${initParam["CSSDirectory"]}'/>style.css />       
    </head>
    <body class="bg-gray" style='overflow-x: hidden'>
        
        <div class="container">    
          <div class="row">
              <div class="col-lg-12 col-md-12 col-md-12 col-sm-12">
                <m:Header pageType="2"></m:Header>
              </div> <%-- Header ends--%>              
                
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 top-element">                    
                  <div class="panel panel-tr">
                      <div class="panel-heading nav panel-heading-tr">
                        <strong class="color-white icon-size">Privacy</strong>                        
                      </div><%-- Panel Heading ends --%>
                      <div class="panel-body bg-gray">
                          <div id="privacy" style="display:none;"><%--Privacy starts--%>
                       <form role="form">                       
                        <div class="form-group small-font-size">
                          <center>
                              <label for="postID" class="control-label start-w start-label">Privacy of posts:</label>
                              <select class="selectpicker dropdown-show" id="postID" data-width="35%" data-style="btn-secondary" required>
                                  <option>All</option>
                                  <option>Friends</option>
                                </select>                            
                            </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                              <label for="postID" class="control-label start-w start-label">Who can write a message:</label>
                              <select class="selectpicker dropdown-show" id="postID" data-width="35%" data-style="btn-secondary" required>
                                  <option>All</option>
                                  <option>Friends</option>
                                </select>                            
                            </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                              <label for="postID" class="control-label start-w start-label">Who can tag me:</label>
                              <select class="selectpicker dropdown-show" id="postID" data-width="35%" data-style="btn-secondary" required>
                                  <option>All</option>
                                  <option>Friends</option>
                                </select>                            
                            </center>
                        </div>
                        <div class="form-group small-font-size">
                          <center>
                              <label for="postID" class="control-label start-w start-label">Profile picture:</label>
                              <select class="selectpicker dropdown-show" id="postID" data-width="35%" data-style="btn-secondary" required>
                                  <option>All</option>
                                  <option>Friends</option>
                                </select>                            
                            </center>
                        </div>
                       <button type="button" role="button" class="btn border-radius bg-tr color-white shadow float-right small-margin">Cancel</button>
                       <button type="submit" role="button" class="btn border-radius bg-tr color-white shadow float-right small-margin">Save</button>
                        
                    </form><br /> <br />
                    <hr class="border-red">
                    <p class="nav-pills-centered">Log in notifications:</p>
                      <div class="radio nav-pills-centered">
                        <label><input type="radio" name="optradio">Email me: @</label>
                      </div>
                      <div class="radio nav-pills-centered">
                        <label><input type="radio" name="optradio">Send a message: +</label>
                      </div>
                      <div class="radio nav-pills-centered">
                        <label><input type="radio" name="optradio">Both</label>
                      </div>
                      <div class="radio nav-pills-centered">
                        <label><input type="radio" name="optradio">None</label>
                      </div>
                   </div><%--Privacy ends--%>
                          <div id="account"style="display:none;"><%--Account settings starts--%>
                            <strong class="margin-left-5">Change your personal information</strong>
                              <form role="form">
                                  <div class="inline-block margin-left-10" style="width:44%">
                                    <label for="nameID" class="control-label start-label account-w-label">Name:</label>
                                    <input type="text" class="form-control account-w inline-block med-margin" id="nameID"><br />
                                    <label for="snameID" class="control-label start-label account-w-label">Surname:</label>
                                    <input type="text" class="form-control account-w inline-block med-margin" id="snameID"><br />
                                    <label for="bdID" class="control-label start-label account-w-label">Birth:</label>
                                    <input type="date" class="form-control account-w inline-block med-margin" id="bddID"><br />
                                  </div>
                                  <div class="inline-block" style="width:45%">
                                    <label for="countryID" class="control-label start-label" style="width: 90px;">Place:</label>
                                    <select class="selectpicker dropdown-show med-margin" id="countryID" data-width="60%">
                                        <option>Azerbaijan</option>
                                        <option>Great Britain</option>
                                        <option>Russia</option>
                                        <option>Spain</option>
                                        <option>Turkey</option>
                                      </select><br />
                                    <label for="emailID" class="control-label start-label account-w-label">Email:</label>
                                    <input type="email" class="form-control account-w inline-block med-margin" id="emailID"><br />
                                    <label for="phoneID" class="control-label start-label account-w-label">Phone:</label>
                                    <input type="number" class="form-control account-w inline-block med-margin" id="phoneID"><br />
                                  </div> <br />                              
                                  <div class="float-right">
                                  <button type="button" role="button" class="btn bg-gray-dark shadow-inset-dark color-white account-btn med-margin">Close</button>
                                  </div>
                                  <div class="float-right inline-block">
                                  <input type="submit" role="button" class="btn shadow-inset account-btn med-margin" value="Save">
                                  </div>
                              </form>
                              <br /> <br /> <br />
                              <hr class="med-margin border-red" /><br />
                              <label for="langacc" class="margin-left-5" style="width: 16%">Language</label>
                                <select class="selectpicker dropdown-show" id="langacc" data-width="27%">
                                  <option>Azerbaijani</option>
                                  <option>English</option>
                                  <option>Russian</option>
                                  <option>Spanish</option>
                                  <option>Turkish</option>
                                </select>
                              <hr class="border-red" style="margin-top: 10%"/>
                              <strong class="margin-left-5">Change your password</strong>
                              <form role="form">
                              <label for="passwordID" class="control-label start-w start-label" style="margin-left: 11%">Current:</label>
                              <input type="password" pattern="[^'\x22\?\/](?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}[^'\x22\?\/]"
                                     class="form-control inline-block account-w-2 med-margin-top" id="passwordID"><br />
                              <label for="password2ID" class="control-label start-w start-label" style="margin-left: 11%">New:</label>
                              <input type="password" pattern="[^'\x22\?\/](?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}[^'\x22\?\/]"
                                     class="form-control inline-block account-w-2 med-margin-top" id="password2ID"><br />
                              <label for="password3ID" class="control-label start-w start-label" style="margin-left: 11%">Retype new:</label>
                              <input type="password" pattern="[^'\x22\?\/](?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}[^'\x22\?\/]"
                                     class="form-control inline-block account-w-2 med-margin-top" id="password3ID">
                              <div class="float-right">
                                  <button type="button" role="button" class="btn bg-gray-dark shadow-inset-dark color-white account-btn med-margin">Close</button>
                              </div>
                              <div class="float-right inline-block">
                                  <input type="submit" role="button" class="btn shadow-inset account-btn med-margin" value="Save">
                               </div>
                              </form><br /> <br /> <br />
                              <hr class="border-red hr-margin" /><br />
                              <a class="news color-tr margin-left-5" href="#">Deactivate the account</a>
                          </div><%--Account Settings ends--%>
                          <div id="block" style="display:none;"><%--Block starts--%>
                              <form role="form">
                               <div class="form-group">
                                   <center>
                                   <label for="blockID" class="control-label start-w start-label">Block a user:</label> 
                                   <input type="text" class="form-control login-input-w inline-block" id="blockID">
                                   <input type="submit" role="button" class="btn shadow-inset med-margin apply-btn" value="Apply">                                   
                                   </center>
                                   <br /><br />
                                   <div style="margin-left: 23%">
                                   <p><strong>List of Blocked users:</strong></p><br />
                                   <ul class="color-tr span-color-black margin-left-5">
                                       <li><span>Filankes <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></span></li>
                                       <li><span>Behmankes <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></span></li>
                                       <li><span>Feshmankes <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></span></li>
                                   </ul>
                                   </div>
                               </div>
                              </form><br /><br />
                              <hr class="hr-margin border-red" />
                              <a href="#" class="color-tr news start-label">Learn more about blocking</a>
                              <a href="#" class="color-tr news">Help Center</a>
                          </div><%--Block ends--%>
                          <div id="help"><%--Help desk starts--%>
                              
                          </div><%--Help desk ends--%>
                      </div>
                </div><%-- Panel ends--%>
              </div><%-- News ends--%>
             <%-- News feed ends--%>
                
            <div class="col-lg-3 col-md-3 hidden-sm hidden-xs" >
              <m:RightCorner pageType="3"></m:RightCorner>
            </div><%-- Login Section ends--%>
                
            </div><%-- Row ends--%>
        </div><%-- Container ends--%>
        
                
        <script src=<c:out value='${initParam["JSDirectory"]}'/>jquery-2.2.3.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>bootstrap.min.js></script>
        <script src=<c:out value='${initParam["CSSDirectory"]}'/>bootstrap-select-1.10.0/dist/js/bootstrap-select.min.js></script>
        <script src=<c:out value='${initParam["JSDirectory"]}'/>functions.js></script>
                
    </body>
</html>


