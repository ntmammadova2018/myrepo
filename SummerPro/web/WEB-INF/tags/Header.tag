<%@tag description="Top Header" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@attribute name="pageType" description="Defines what should be included on the Top Header" 
             required="true" rtexprvalue="true"%>

<style>
    #logo{
        margin-left:16%
    }
    #logo2{
        margin-right:9%
    }
    .nvbtn{
        margin-top: 4px;
    }
    .infbtn{
        font-size: 25px;
        padding:5px;
    }
    .chbttn{
        font-size: 60px;
        margin-top:250%
    }
    .nvinp{
        display: inline-block;
        margin: 10px;
        margin-right: 80px;
        width:250px;
    }
    .sidebar-nav{
        margin-right: 0
    }
</style>

<div class="nav navbar no-margin page-header navbar-fixed-top" style="background-color: rgba(240, 239, 240,0.71);">
    <hr class="border-red no-margin" style="margin-bottom:10px;border:5px solid">
    <center>
        <c:choose>
          <c:when test="${pageType eq 1}" >
           <a href="Home.jsp">
            <img id="logo" class="logo-size" src=<c:out value='${initParam["CSSDirectory"]}'/>logo.png alt="Egimus Logo">
           </a>
            <form class="navbar-search pull-right">                     
               <input type="text" class="form-control input-sm no-border underline nvinp" placeholder="...">
            </form>
           </c:when>
           <c:when test="${pageType eq 2}" >
            <a href="Home.jsp">
            <img id="logo2" class="logo-size" src=<c:out value='${initParam["CSSDirectory"]}'/>logo.png alt="Egimus Logo">
            </a>
           </c:when>
        </c:choose>
    </center>
    </div>
</div>