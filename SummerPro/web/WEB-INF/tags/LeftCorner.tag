<%@tag description="Right Corner of the Page" pageEncoding="UTF-8"%>
<%-- Tags --%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="m" tagdir="/WEB-INF/tags" %>

<%@attribute name="pageType" description="Defines which components should be used on the left side" 
             required="true" rtexprvalue="true"%>

<div class="sidebar-nav-fixed affix affix-w side-overflow top-element"><!-- there was affix-w -->
    <div style="text-align: center">
       <ul class="nav nav-pills inline-block bg-gray-onhover panel-max-height-long" style="margin:auto;">
        <li>
         <a href="BusinessPage.jsp"><span class="glyphicon glyphicon-briefcase rcbtn icon-size color-tr" aria-hidden="true"></span></a>
        </li>
        <li>
         <a href="Profile.jsp"><span class="fa fa-user rcbtn icon-size color-tr" aria-hidden="true"></span></a>
        </li>
        <li>
         <a href="Home.jsp"><span class="glyphicon glyphicon-home rcbtn icon-size color-tr" aria-hidden="true"></span></a>
        </li>
       </ul>
    </div>
    <%-- Header of Left Corner ends --%>
    <c:choose>
    <c:when test="${pageType eq 1}" >
        <m:Profile></m:Profile>
    </c:when>
    <c:when test="${pageType eq 2}" >
        <m:UserProfile></m:UserProfile>
    </c:when>
    <c:when test="${pageType eq 3}" >
        <m:BusinessProfile></m:BusinessProfile>
    </c:when>
    </c:choose>
    <m:OnlineList></m:OnlineList>
    <p class="small-font-size">&copy; <time datetime="2016">2016 Egimus</p>
</div>