<%@tag description="User Profile" pageEncoding="UTF-8"%>

<div class="panel panel-tr">
   <div class="panel-heading panel-heading-tr">
     <a href="Profile.jsp">
         <div class="media">
           <div class="media-left">
               <img class="img-circle thumb-size"
                src="http://science-all.com/images/wallpapers/random-image/random-image-6.jpg"
                alt="Profile photo">
           </div>
          <div class="media-body">
           <div class="media-heading inline-block">
               <strong class="color-white">DinoZaur <wbr/><span style="margin: 0!important">Baxsheliyev</span></strong>
           </div>
          </div>
        </div>     
     </a>
    </div>                        
    <div class="panel-body overflow" style="height:220px">
     <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">
          <a href="#">Link</a>
        </div>
      </div>
    </div> <%-- Ordinary link format --%>
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">
          <a href="#">Link</a>
        </div>
      </div>
    </div>
    <div class="panel-group" id="accordion">
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#pagescollapse">My Pages</a>
        </div>
      </div>
      <div id="pagescollapse" class="panel-collapse collapse">
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
      </div>
    </div>
    <div class="panel">
      <div class="panel-heading">
        <div class="panel-title">
          <a data-toggle="collapse" data-parent="#accordion" href="#groupscollapse">My Groups</a>
        </div>
      </div>
      <div id="groupscollapse" class="panel-collapse collapse">
        <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipisicing elit,
        sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</div>
      </div>
    </div>
  </div><%-- Accordion ends--%>
 </div><%-- Body of listing ends--%>
</div>