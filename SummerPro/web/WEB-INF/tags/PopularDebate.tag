<%@tag description="Debate Sections" pageEncoding="UTF-8"%>

<div class="panel debate-panel-height">
    <div class="panel-heading text-danger">
       <span class="text-danger">Debates</span>
        <a data-toggle="modal" data-target="#createDebate"><span class="glyphicon glyphicon-plus-sign small-font-size float-right" aria-hidden="true"></span></a>
        <hr class="border-red no-margin">        
    </div>
    <div class="panel-body">
  <ul class="nav nav-tabs small-font-size">
    <li class="active"><a class="text-danger" data-toggle="tab" href="#trend">Top</a></li>
    <li><a class="text-danger" data-toggle="tab" href="#friend">Friends'</a></li>
    <li><a class="text-danger" data-toggle="tab" href="#join">Joined</a></li>
  </ul>

  <div class="tab-content">
   <div id="trend" class="tab-pane fade in active">
   <div class="panel panel-default">
    <div class="panel-body panel-max-height overflow">
       
           <a class="color-black" href="#"><strong>Name</strong></a>
           <p>Text</p>
           <hr class="hr-margin">
          
          <a class="color-black" href="#"><strong>Name</strong></a>
          <p>Text</p>
          <hr class="hr-margin">
    </div>
    <div class="panel-footer">
      <div class="text-center">
      <button type="button" role="button" class="btn btn-block dbtn">Show more</button>
    </div>
   </div> 
   </div> <%-- Panel ends--%>
    </div> <%-- Tab Content ends--%>
    <div id="friend" class="tab-pane fade">
      <div class="panel panel-default">
    <div class="panel-body">
     
         <a class="color-black" href="#"><strong>Name</strong></a>
         <p>Text</p>
         <hr class="hr-margin">
        
          <a class="color-black" href="#"><strong>Name</strong></a>
          <p>Text</p>
          <hr class="hr-margin">
    </div>
    <div class="panel-footer">
      <div class="text-center">
      <button type="button" role="button" class="btn btn-block dbtn">Show more</button>
    </div>
   </div> 
   </div> <%-- Panel ends--%>
  </div> <%-- Tab Content ends--%>
    <div id="join" class="tab-pane fade">
     <div class="panel panel-default">
    <div class="panel-body">
    
         <a class="color-black" href="#"><strong>Name</strong></a>
         <p>Text</p>
         <hr class="hr-margin">
       
          <a class="color-black" href="#"><strong>Name</strong></a>
          <p>Text</p>
          <hr class="hr-margin">
    </div>
    <div class="panel-footer">
      <div class="text-center">
      <button type="button" role="button" class="btn btn-block dbtn">Show more</button>
    </div>
   </div> 
   </div> <%-- Panel ends--%>
   </div> <%-- Tab Content ends--%>
  </div> <%-- Tab ends--%>
 </div>
</div>  