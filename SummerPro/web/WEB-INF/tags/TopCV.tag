<%@tag description="Top CVs" pageEncoding="UTF-8"%>

<div class="panel">
    <div class="panel-heading text-danger">
     <a href="#">
     <strong class="text-danger">Top CVs</strong></a>
     <hr class="border-red hr-margin">
    </div>
    <div class="panel-body">
        <ul class="unstyled" style="padding-left: 10% !important;">
            <li class="med-margin-bottom"><a href="#">
             <div class="media">
                <div class="media-left">
                    <img class="img-circle thumb-size"
                     src="..."
                     alt="Profile photo">
                </div>
                <div class="media-body">
                     <strong>Developer</strong>
                </div>
            </div></a></li>
            <li class="med-margin-bottom"><a href="#">
                <div class="media">
                <div class="media-left">
                    <img class="img-circle thumb-size"
                     src="..."
                     alt="Profile photo">
                </div>
                <div class="media-body">
                     <strong>Translator</strong>
                </div>
                </div></a></li>
            <li class="med-margin-bottom"><a href="#">
                <div class="media">
                <div class="media-left">
                    <img class="img-circle thumb-size"
                     src="..."
                     alt="Profile photo">
                </div>
                <div class="media-body">
                     <strong>Designer</strong>
                </div>
                </div></a></li>
            <li class="med-margin-bottom"><a href="#">
                <div class="media">
                <div class="media-left">
                    <img class="img-circle thumb-size"
                     src="..."
                     alt="Profile photo">
                </div>
                <div class="media-body">
                     <strong>Artist</strong>
                </div>
                </div></a></li>
        </ul>
    </div>
</div>